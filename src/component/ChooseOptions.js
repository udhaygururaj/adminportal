import React from 'react';
import InviteUser from './InviteUser';
import ReserveCode from './ReserveCode';
import Select from '../common/Select';
class ChooseOptions extends React.Component{
    constructor(props){
        super(props);
        this.state={
            option:'invite'
        }
        this.updateOptions = this.updateOptions.bind(this);
    }
    updateOptions(event){
        this.setState({ option : event.target.value});
    }

    render(){
        const { option } = this.state;
        return(
            <div className='option-container'>
                {
                    <Select
                        options = {[{displayName:'InviteUser', value:'invite'},
                                    {displayName:'ReserveCode', value:'reserve'}]}
                        updateState={this.updateOptions}
                        opValue = {option}
                    />
                }
                {
                    option === 'invite' && <InviteUser/>
                }
                {
                    option === 'reserve' && <ReserveCode/>
                }
            </div>
        );
    }
}

export default ChooseOptions;