import React, { Component } from 'react';
import autoBind from 'auto-bind';
import TextInput from '../common/TextInput';
import Label from '../common/Label';
import InviteUserStyle from './inviteUser.css';
import Button from '../common/AppButton';
class InviteUser extends Component {
    constructor(props){
        super(props);
        autoBind(this);
    }
    render(){
        return(
            <div className="container">
                <div className="row">
                    <div className="col offset-md-4">
                        <Label 
                            name='Reserve Code' 
                            styleName='label-invite-header'
                        />
                    </div>
                
                </div>
            <div className='row offset-md-2 image-display'>
                <img src='../../img1.JPG' height='192px' width='72%'/>
            </div>
            <div className='input-container'>
            <div className="row">
                    <div className="col-4 offset-md-2">
                            <TextInput
                                labelName='First Name'
                                styleName='input-firstname'
                            />
                    </div>
                    <div className="col-5"> 
                            <TextInput
                                labelName='Last Name'
                                styleName='input-lastName'
                            />
                    </div>
            </div>
            <div className='row'>
                    <div className="col-8 offset-md-2">
                            <TextInput
                                labelName='Email'
                                styleName='input-email'
                            />
                    </div>
            </div>
            <div className='row'>
                    <div className="col-8 offset-md-2">
                            <Button
                                styleName='btn-common'
                                btnName='Reserve'
                            />
                    </div>
            </div>
            </div>
        </div>
          
        )
    }
}

export default InviteUser;