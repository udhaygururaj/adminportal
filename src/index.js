import React from 'react';
import ReactDOM from 'react-dom';
// import InviteUser from './component/InviteUser';
import ChooseOptions from './component/ChooseOptions';
import { Provider } from 'react-redux'
import store from './store/index';

ReactDOM.render(
    <Provider store={store}>
        <ChooseOptions/>
    </Provider>, 
    document.getElementById('app')
);
