import React from 'react';
import { NumericTextBox, Input, Switch } from '@progress/kendo-react-inputs';

const TextInput = (props) => {
    return (
        <div className={`${props.styleName}-container`}>
            <Input 
                label={props.labelName} 
                className={props.styleName}
            />
        </div>
    )
}

export default TextInput;