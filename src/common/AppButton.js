import React from 'react';
import { Button } from '@progress/kendo-react-buttons';

const AppButton = (props) => {
    return (
        <div className={`${props.styleName}-container`}>
            <Button 
                primary={true}
                className={props.styleName}
            >
                {props.btnName}
            </Button>
        </div>
    )
}

export default AppButton;