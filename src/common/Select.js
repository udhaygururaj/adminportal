import React from 'react';

const Select = (props) => {
return(
    <select value={props.opValue} onChange={props.updateState}>
        {
            props.options.map( (op) => {
                return <option value={op.value}>{op.displayName}</option>
            })
        }

    </select>
)
}

export default Select;