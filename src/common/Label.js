import React from 'react';

const Label = (props) => {
    return (
        <div className={props.styleName}>
            <span>{props.name}</span>
        </div>
    )
}

export default Label;