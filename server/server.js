var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var getlicenses = require('./json/getlicenses') ;
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080; 

var router = express.Router();

router.get('/licenses', function(req, res) {
    res.json(getlicenses);   
});

router.get('/licenses/:uuid', function(req, res) {
  console.log('uuid', req.params.uuid);
  res.json(getlicenses);   
});

app.use('/api', router);

app.listen(port);
console.log('Sever listens on port ' + port);